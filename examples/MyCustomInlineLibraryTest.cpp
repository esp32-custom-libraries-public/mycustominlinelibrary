#include <Arduino.h>
#include <MyCustomInlineLibrary.h>


MyCustomInlineLibrary myLibrary = MyCustomInlineLibrary();

String greeting;


void setup() {
    Serial.begin(9600);

    myLibrary.writeString("Hello world");

    greeting = myLibrary.readString();
}


void loop() {
    Serial.println("\t" + greeting);
    delay(2000);
}
