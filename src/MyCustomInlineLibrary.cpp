#include "MyCustomInlineLibrary.h"

MyCustomInlineLibrary::MyCustomInlineLibrary() {} // constructor

void MyCustomInlineLibrary::writeString(String str) {
    _str = str;
    return;
}

String MyCustomInlineLibrary::readString() {
    return _str;
}
