#ifndef MyCustomInlineLibrary_h
    #define MyCustomInlineLibrary_h


    #if ARDUINO >= 100
        #include "Arduino.h"
    #else
        #include "WProgram.h"
        #include "pins_arduino.h"
        #include "WConstants.h"
    #endif


    class MyCustomInlineLibrary {

        public:
            MyCustomInlineLibrary(); // constructor

            void writeString(String str);
            String readString();

        private:
            String _str;
    };


#endif
